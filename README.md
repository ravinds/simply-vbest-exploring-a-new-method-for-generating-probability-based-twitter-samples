# Simply VBEST: Exploring a new method for generating probability-based twitter samples

• The aim is to improve the data quality of the twitter data, which has an extreme bias otherwise due to constraints like maximum number of queries one can use per twitter account per day.

• Team developed an algorithm that creates a collection of Primary Sampling Units (PSUs) of time blocks in a given day.

• Created a R function library for Loess Smoother and Adaptive-Splines on the tweet velocity data which then constructs PSUs.

• Parallelized code for Loess predictions on large input x-vectors to speed up the computations by approximately 5 folds.

• Built functions for the CDF, Inverse CDF for Loess and Adaptive-Spline fits of order 1 and 3.

• Implemented Systematic Sampling using Empirical Inverse CDF for Loess and Analytical Inverse CDF for Adaptive-Splines.

***Techniques Used:*** Loess Smoothing, Adaptive-Splines, RPy2, doParallel, tidyverse, Scikit-learn, Cross-Validation (**R**, **Python**)
