library(aspline)
library(tidyverse)
library(splines2)
library(pracma)
library(bootstrap)
library(Bolstad2)
library(rootSolve)
library(PerMallows)
library(textir)
library(itertools)
library(foreach)
library(MESS)
library(doParallel)

velocity1 = read.csv("velocity1.csv")
velocity2 = read.csv("velocity2.csv")
velocity3 = read.csv("velocity3.csv")
data = velocity3
x <- data$Hour
y <- data$Velocity

# Aspline_Area_Proportions function

Aspline_Area_Proportions <- function(degree,df=data,k=20,lower=0,upper=86400,l=1000,Plot=FALSE){
  x <- df$Hour
  y <- df$Velocity
  knots <- seq(lower, upper, length = k + 2)[-c(1, k + 2)]
  pen <- 10 ^ seq(-2, 2, 0.25)
  x_seq <- seq(lower, upper, length = l)
  aridge <- aridge_solver(x, y, knots, pen, degree = degree)
  a_fit <- lm(y ~ bSpline(x, knots = aridge$knots_sel[[which.min(aridge$ebic)]], degree = degree))
  
  a_predict <- data_frame(x = x_seq, pred = predict(a_fit, data.frame(x = x_seq)))
  
  if (Plot == TRUE){
    plot <- ggplot() +
      geom_point(data = data, aes(x, y), shape = 1) +
      geom_line(data = a_predict, aes(x, pred, colour="red"), size = 0.9) +
      geom_vline(xintercept = aridge$knots_sel[[which.min(aridge$ebic)]], color = 'blue',linetype="dotted", size = 0.75) +
      scale_color_hue(labels = c("Piecewise Spline Fits")) + 
      theme(legend.position = "top") +
      guides(color=guide_legend("")) +
      ylab("Predicited Velocity") +
      xlab("Time of the day (in seconds)")
  }
  
  else if (Plot == FALSE){
    plot = "Please set Plot argument as TRUE to display plot"
  }
  
  final_knots <- aridge$knots_sel[[which.min(aridge$ebic)]] # Knots
  
  # Calulating Analytical area under the piecewise Aspline-degree 1
  
  if (degree==1){
    x_points = c(0,final_knots,86400)
    intercepts = rep(0, length(final_knots)+1)
    slopes = rep(0, length(final_knots)+1)
    
    for (i in 1:length(intercepts)) {
      df <- subset(a_predict, x >= x_points[i] & x <= x_points[i+1])
      tempmodel_coefs <- coef(lm(pred ~ x, data=df))
      intercepts[i] = tempmodel_coefs[1]
      slopes[i] = tempmodel_coefs[2]
    }
    
    areas = rep(0, length(final_knots)+1)
    
    for (i in 1:length(areas)) {
      func = function(x,k=i) {return(slopes[k]*x + intercepts[k])}
      areas[i] = integral(func, x_points[i], x_points[i+1])
    }
    
    relative_proportions <- (1/sum(areas))*areas
    
    return_list <- list(relative_proportions,final_knots,plot,intercepts,slopes,sum(areas))
  }
  
  # Calulating Analytical area under the piecewise Aspline-degree 3
  
  if (degree==3){
    x_points = c(0,final_knots,86400)
    intercepts = rep(0, length(final_knots)+1)
    first = rep(0, length(final_knots)+1)
    second = rep(0, length(final_knots)+1)
    third = rep(0, length(final_knots)+1)
    
    for (i in 1:length(intercepts)) {
      df <- subset(a_predict, x >= x_points[i] & x <= x_points[i+1])
      tempmodel_coefs <- coef(lm(pred ~ x + I(x^2) + I(x^3),data=df))
      intercepts[i] = tempmodel_coefs[1]
      first[i] = tempmodel_coefs[2]
      second[i] = tempmodel_coefs[3]
      third[i] = tempmodel_coefs[4]
    }
    
    areas = rep(0, length(final_knots)+1)
    
    for (i in 1:length(areas)) {
      func = function(x,k=i) {return(third[k]*x^3 + second[k]*x^2 + first[k]*x + intercepts[k])}
      areas[i] = integral(func, x_points[i], x_points[i+1])
    }
    
    relative_proportions <- (1/sum(areas))*areas
    
    return_list <- list(relative_proportions,final_knots,plot,intercepts,first,second,third,sum(areas))
    
  }
  return(return_list)
}


# Loess_Area_Proportions function

loess_wrapper_extrapolate <- function(df=data, span.vals = seq(0.2, 0.65, by = 0.05), folds = 3){
  
  x <- df$Hour
  y <- df$Velocity
  
  # Do model selection using mean absolute error, which is more robust than squared error.
  mean.abs.error <- numeric(length(span.vals))
  
  # Quantify error for each span, using CV
  loess.model <- function(x, y, span){
    loess(y ~ x, span = span, control=loess.control(surface="direct"))
  }
  
  loess.predict <- function(fit, newdata) {
    predict(fit, newdata = newdata)
  }
  
  span.index <- 0
  for (each.span in span.vals) {
    span.index <- span.index + 1
    y.hat.cv <- crossval(x, y, theta.fit = loess.model, theta.predict = loess.predict, span = each.span, ngroup = folds)$cv.fit
    non.empty.indices <- !is.na(y.hat.cv)
    mean.abs.error[span.index] <- mean(abs(y[non.empty.indices] - y.hat.cv[non.empty.indices]))
  }
  
  # find the span which minimizes error
  best.span <- span.vals[which.min(mean.abs.error)]
  
  # fit and return the best model
  best.model <- loess(y ~ x, span = best.span, control=loess.control(surface="direct"))
  return(list(best.model,best.span))
}

set.seed(2020)

loess.model = loess_wrapper_extrapolate()

# loess.model[[2]]

Loess_x_grid = seq(0,86400,by=0.5)

FinalLoessFit <- predict(loess.model[[1]],Loess_x_grid)

Loess_Total_Area <- function(type, model=loess.model[[1]], grid = Loess_x_grid){
  
  if (type == "T"){
    return(trapz(Loess_x_grid, FinalLoessFit))
  }
  
  else if (type == "S"){
    return(sintegral(Loess_x_grid, FinalLoessFit)$int)
  }
}

# Getting the normalized full function form (PDF) with help of indicators.

Normalized_fullfunction_Aspline1 <- function(t){
  
  list = Aspline_Area_Proportions(1)
  knot_list = list[[2]]
  intercepts = list[[4]]
  slopes = list[[5]]
  
  x_points = c(0,knot_list,86400)
  
  func_value = 0
  for (i in 1:(length(knot_list)+1)){
    func_value = func_value + (slopes[i]*t + intercepts[i])*as.numeric(I(t >= x_points[i] & t <= x_points[i+1]))
  }
  
  return((1/Aspline_Area_Proportions(1)[[6]])*func_value)
}


Normalized_fullfunction_Aspline3 <- function(t){
  
  list <- Aspline_Area_Proportions(3)
  knot_list = list[[2]]
  intercepts = list[[4]]
  first = list[[5]]
  second = list[[6]]
  third = list[[7]]
  
  x_points = c(0,knot_list,86400)
  
  func_value = 0
  for (i in 1:(length(knot_list)+1)){
    func_value = func_value + (third[i]*t^3 + second[i]*t^2 + first[i]*t + intercepts[i])*as.numeric(I(t >= x_points[i] & t <= x_points[i+1]))
  }
  
  return((1/Aspline_Area_Proportions(3)[[8]])*func_value)
}

Normalized_piecewise_Aspline1 <- function(t,k){
  
  list = Aspline_Area_Proportions(1)
  intercepts = list[[4]]
  slopes = list[[5]]
  
  return((1/Aspline_Area_Proportions(1)[[6]])*(slopes[k]*t + intercepts[k]))
}

Normalized_piecewise_Aspline3 <- function(t,k){
  
  list = Aspline_Area_Proportions(3)
  intercepts = list[[4]]
  first = list[[5]]
  second = list[[6]]
  third = list[[7]]
  
  return((1/Aspline_Area_Proportions(3)[[8]])*(third[k]*t^3 + second[k]*t^2 + first[k]*t + intercepts[k]))
}

# Getting CDF's equations setup

CDF_Aspline1 <- function(t){
  return(integral(Normalized_fullfunction_Aspline1, min(x), t))
}

CDF_Aspline3 <- function(t){
  return(integral(Normalized_fullfunction_Aspline3, min(x), t))
}

Inverse_CDF_Aspline1 <- function(r){
  return(uniroot(function(t){return(integral(Normalized_fullfunction_Aspline1, min(x), t)-r)},lower = min(x), upper = max(x), tol = 1e-9)$root)
}

Inverse_CDF_Aspline3 <- function(r){
  return(uniroot(function(t){return(integral(Normalized_fullfunction_Aspline3, min(x), t)-r)}, lower = min(x), upper = max(x), tol = 1e-9)$root)
}

# Improvement to increase the computation speed (using polyroot() function)

Speedy_Inverse_CDF_Aspline1 <- function(r){
  
  list = Aspline_Area_Proportions(1)
  area_props = list[[1]]
  knot_list = list[[2]]
  intercepts = (1/list[[6]])*list[[4]]
  slopes = (1/list[[6]])*list[[5]]
  x_points = c(0,knot_list,86400)
  
  cumul_area_props <- append(cumsum(area_props), 0, after = 0)
  
  for (i in 1:(length(cumul_area_props)-1)){
    if (cumul_area_props[i] <= r & cumul_area_props[i+1] >= r){
      coefficients_quadratic_poly = c(-(0.5*slopes[i]*(x_points[i])^2 + intercepts[i]*x_points[i] + r - cumul_area_props[i]), intercepts[i], 0.5*slopes[i])
      roots_vector = polyroot(coefficients_quadratic_poly)
      counter = i # This is just to keep track of the stratum where r falls. It will be useful to return the correct root later.
    }
  }
  
  for (j in 1:length(roots_vector)){
    if (round(Re(roots_vector[j]),2) >= x_points[counter] & round(Re(roots_vector[j]),2) <= x_points[counter+1]){
      root = Re(roots_vector[j])
    }
    
    # Since the imaginary roots always exists in pair, we would always have real roots.
    
  }
  return(root)
}

Speedy_Inverse_CDF_Aspline3 <- function(r){
  
  list = Aspline_Area_Proportions(3)
  area_props = list[[1]]
  knot_list = list[[2]]
  intercepts = (1/list[[8]])*list[[4]]
  first = (1/list[[8]])*list[[5]]
  second = (1/list[[8]])*list[[6]]
  third = (1/list[[8]])*list[[7]]
  x_points = c(0,knot_list,86400)
  
  cumul_area_props <- append(cumsum(area_props), 0, after = 0)
  
  for (i in 1:(length(cumul_area_props)-1)){
    if (cumul_area_props[i] <= r & cumul_area_props[i+1] >= r){
      coefficients_quartic_poly = c(-(0.25*third[i]*(x_points[i])^4 + (1/3)*second[i]*(x_points[i])^3 + 0.5*first[i]*(x_points[i])^2 + intercepts[i]*x_points[i] + r - cumul_area_props[i]), intercepts[i], 0.5*first[i], (1/3)*second[i], 0.25*third[i])
      roots_vector = polyroot(coefficients_quartic_poly)
      counter = i # This is just to keep track of the stratum where r falls. It will be useful to return the correct root later.
    }
  }
  
  for (j in 1:length(roots_vector)){
    if (round(Re(roots_vector[j]),2) >= x_points[counter] & round(Re(roots_vector[j]),2) <= x_points[counter+1] & round(Im(roots_vector[j])) == 0){
      root = Re(roots_vector[j])
    }
  }
  
  return(root)
}

Sampling <- function(k){
  
  set.seed(100)
  random_numbers <- runif(k)
  corresponding_x <- vector()
  
  for (i in 1:length(random_numbers)){
    corresponding_x <- c(corresponding_x,Speedy_Inverse_CDF_Aspline3(random_numbers[i]))
  }
  
  return(round(corresponding_x,3))
}




