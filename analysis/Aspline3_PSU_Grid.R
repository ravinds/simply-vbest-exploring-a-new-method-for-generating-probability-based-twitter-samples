
list3 <- Aspline_Area_Proportions(3)

Aspline3_Total_Area <- integral(fullfunction_Aspline,0,86400,degree=3)

Aspline3_possible_targets <- c(96,97,98,99,100,101,102,103,104)

Aspline3_remainders <- vector()

for (i in 1:length(Aspline3_possible_targets)){
  Aspline3_remainders <- c(Aspline3_remainders,Aspline3_Total_Area%%Aspline3_possible_targets[i])
}

for (i in 1:length(Aspline3_remainders)){
  if (min(Aspline3_remainders) == 0){
    Aspline3_volume_target <- Aspline3_possible_targets[which(Aspline3_remainders == 0)[1]]
    Aspline3_num_psu <- Aspline3_Total_Area/Aspline3_volume_target
  }
  
  else {
    Aspline3_volume_target <- Aspline3_possible_targets[which(Aspline3_remainders == max(Aspline3_remainders))[1]]
    Aspline3_num_psu <- floor(Aspline3_Total_Area/Aspline3_volume_target)+1
  }
}

Aspline3_psu_grid <- c(86400)

for (i in 1:(Aspline3_num_psu-1)){
  Aspline3_psu_grid <- c(Aspline3_psu_grid,Aspline_next_x_finder(Aspline3_psu_grid[i],degree=3))
}

rounded_off_Aspline3_psu_grid <- round(Aspline3_psu_grid,3)

